Djangology CfMS 
===============

Djangology Crowdfunding Management System (Djangology CfMS) is a Content Management System wrote in [Django Framework](https://www.djangoproject.com/) (based on [Python](https://en.wikipedia.org/wiki/Python_%28programming_language%29)) designed for allow organizations to build themselves microcrowdfundings sites.

Djangology CfMS is a fork of [FreedomSponsors](https://github.com/freedomsponsors/www.freedomsponsors.org). Djangology CfMS is written specially for support [Funding.Wiki](https://funding.wiki) adding features related to non-software projects.

# Installation instructions
Clone the project

*Extra instructions in* `doc/setup.md`

`git clone https://gitlab.com/wikifunding/djangology-cfms && cd djangology-cfms`

## 1. Install dependencies 

### 1.1 Debian
```bash
sudo apt-get install postgresql postgresql-contrib \
python-dev python-lxml libxslt-dev libpq-dev python-pip \
libtiff5-dev libjpeg62-turbo-dev zlib1g-dev libfreetype6-dev \
liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk \
libxmlsec1-dev redis-server
```
### 1.2 Ubuntu
```bash
sudo apt-get install postgresql-server-dev-9.6 postgresql-9.6 \
python-dev python-lxml libxslt-dev libpq-dev pgadmin3 \
libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev \
liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk \
python-pip redis-server
```
(@Acromantula: possibly `libxmlsec1-dev` needed, too - I was propted while trying to install `dm.xmlsec.binding==1.3.2` in requirements.txt)
        
If you need it, see [how to install old version of postgresql](https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/62)
    
## 2. Create database:
```bash
sudo su postgres #run the next command as postgres
createuser -d -SRP frespo # this will prompt you to create a password (just use frespo for now)
createdb -O frespo frespo
exit # go back to your normal user
```

## 3. Create virtual environment:
```bash
pip install virtualenv
sudo /usr/bin/easy_install virtualenv
virtualenv bin
```

If you want to activate the virtual environment just
```bash
source bin/bin/activate
```
	
Alternativelly you can just create it using the **pycharm** wizard:
*  ctrl+alt+s > search project interpreter > 'settings wheel' > add (look on bin/bin/python2.7 file)

## 4. Install `requirements.txt` inside virtual env:
```bash
pip install -r requirements.txt 
```

See installation dependencies errors at the end of this document.

## 5. Create database
Use `migrate` instead of `syncdb` command as shown in original instructions.
```bash
cd djangoproject
python manage.py migrate
```

## 6. fill the table "core_languages" with language list
This command will fetch language list from the server and then insert into the database.
```bash
cd djangoproject
python manage.py fillLanguageData
```

## (optional) Enable SSL for localhost
If you're running Djangology-CfMS in your localhost machine for development reasons you want to enable a SSL certificate in the Django Project and in your browser to avoid `urlopen error [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed` (other way to do it is [disabling security certificate checks for requests in Python](https://www.geeksforgeeks.org/how-to-disable-security-certificate-checks-for-requests-in-python/), but in that case you could not test SSL features).

Source: https://medium.com/@millienakiganda/creating-an-ssl-certificate-for-localhost-in-django-framework-45290d905b88
1. Mkcert (cert and key filenames & domains configurable)
```bash
# install mkcert
sudo apt install libnss3-tools # install certutil dependency
curl -JLO "https://dl.filippo.io/mkcert/latest?for=linux/amd64"
chmod +x mkcert-v*-linux-amd64
sudo cp mkcert-v*-linux-amd64 /usr/local/bin/mkcert
mkcert -install
cd djangoproject/
mkcert -cert-file cert.pem -key-file key.pem 0.0.0.0 localhost 127.0.0.1 ::1
# RESTART BROWSER
```
2. Django-sslserver
```bash
pip install django-sslserver # with correct virtual environment already active
```
Add `sslserver` to `INSTALLED_APPS` in `djangoproject/frespo/settings.py`

## 6. Run backend server
```bash
./manage.py runserver
```
or with SSL
```bash
./manage.py runsslserver --certificate cert.pem --key key.pem  # correct file names if changes
```
	
## 7. Install and run frontend server
On `djangoproject` directory:
```bash
sudo apt install nodejs npm
sudo npm install grunt-cli -g
npm install
grunt build
```

# Optional extra set up
## Use another settings file
*  Copy `cp frespo/settings.py frespo/settings_dev.py` and
*  add the url to ALLOWED_HOSTS. There are two ways you can do that:
   * set the environment variable `DJANGO_SETTINGS_MODULE=frespo.settings_dev`; or
   * when running `manage.py` add `--settings=frespo.settings_dev` 

## Set up **PyCharm** to work with django applications.
Extracted from https://automationpanda.com/2017/09/14/django-projects-in-pycharm-community-edition/ :

From the Run menu, select Edit Configurations…. Click the plus button in the upper-left corner to add a Python configuration. Give the config a good name (like “Django: <command>”). Then, set Script to `manage.py` and Script parameters to the name and options for the desired Django admin command (like “runserver”). Set Working directory to the absolute path of the project root directory. Make sure the appropriate Python SDK is selected and the PYTHONPATH settings are checked. Click the OK button to save the config. The command can then be run from Run menu options or from the run buttons in the upper-right corner of the IDE window. You can add `--settings=frespo.settings_dev` there also
    
## Configure environment variables to make the application working.
You can configure it into PyCharm on `Run/Debug configurations > Environment > Environment variables > Click on the icon and add there the variables`. Now a list of some usefull variables:
```
GITHUB_APP_ID # The id to login with your github account
GITHUB_API_SECRET # The secret to login with your github account
```
	
#### Dependencies installations errors:

Look at https://gitlab.com/wikifunding/djangology-cfms/wikis/home#installing-on-new-environments for further information

* `ImportError: No module named _markerlib, Failed building wheel for distribute`:
	```
	easy_install distribute
	```

* `Error: could not determine PostgreSQL version from '11.1'`
	Need to downgrade postgres and psql. Uninstall all versions above 9.6. To see how to install 9.6 version see: https://gitlab.com/CommonsVisualization/augmented-cultural-experience/issues/62
	```
	sudo apt purge postgresql-server-dev-10 postgresql-server-dev-10 postgresql-server-dev-11 postgresql-server-dev-10 postgresql-client-11 postgresql-client-10
	```
	
* `Failed building wheel for uWSGI`
	Update `requirements.txt` to use uWSGI version  2.0.15.
	 
* `ImportError: No module named unipath`
    ```
    pip install unipath 
    ```

# About name

Djangology is a jazz standard compositions made by [Django Reinhardt](http://en.wikipedia.org/wiki/Django_Reinhardt) who is the original guitarrist from the Django framework get [the name](http://www.djangobook.com/en/2.0/chapter01.html#django-s-history).

# Thanks

To FreedomSponsors developers and Free Software community involved in big projects like Python, Django and PostgreSQL that make possible this project. A special thanks goes out to Tony Lampada for [FreedomSponsors development](https://github.com/freedomsponsors/www.freedomsponsors.org).

# Licensing

This software is licensed under the [Affero General Public License](http://www.gnu.org/licenses/agpl-3.0.html). Take care of the freedom of yours users when running this software.
