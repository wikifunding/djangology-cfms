.
├── arxiu.txt
├── bitcoin_frespo
│   ├── admin.py
│   ├── admin.pyc
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── management
│   │   ├── commands
│   │   │   ├── __init__.py
│   │   │   └── refillReceiveAddressPool.py
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0001_initial.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── models.py
│   ├── models.pyc
│   ├── services
│   │   ├── bitcoin_services.py
│   │   ├── bitcoin_services.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   └── utils
│       ├── bitcoin_adapter.py
│       ├── bitcoin_adapter.pyc
│       ├── __init__.py
│       └── __init__.pyc
├── bitcoin_jobs.sh
├── cert.pem
├── cleanup.sh
├── compilemessages.sh
├── core
│   ├── admin.py
│   ├── admin.pyc
│   ├── context_processors.py
│   ├── context_processors.pyc
│   ├── decorators.py
│   ├── decorators.pyc
│   ├── forms.py
│   ├── forms.pyc
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── management
│   │   ├── commands
│   │   │   ├── bitcoin_jobs.py
│   │   │   ├── __init__.py
│   │   │   ├── loadFeedbackData.py
│   │   │   ├── loadIssueDescriptions.py
│   │   │   ├── loadProjects.py
│   │   │   ├── payTest.py
│   │   │   ├── populateTest.py
│   │   │   └── screen2username.py
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── mapa_do_site.txt
│   ├── middlewares.py
│   ├── middlewares.pyc
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0001_initial.pyc
│   │   ├── 0002_djangology.py
│   │   ├── 0002_djangology.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── models.py
│   ├── models.pyc
│   ├── serializers.py
│   ├── serializers.pyc
│   ├── services
│   │   ├── activity_services.py
│   │   ├── activity_services.pyc
│   │   ├── bitcoin_frespo_services.py
│   │   ├── bitcoin_frespo_services.pyc
│   │   ├── comment_services.py
│   │   ├── comment_services.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── issue_services.py
│   │   ├── issue_services.pyc
│   │   ├── mail_services.py
│   │   ├── mail_services.pyc
│   │   ├── media_services.py
│   │   ├── media_services.pyc
│   │   ├── paypal_services.py
│   │   ├── paypal_services.pyc
│   │   ├── revision_services.py
│   │   ├── revision_services.pyc
│   │   ├── stats_services.py
│   │   ├── stats_services.pyc
│   │   ├── tag_services.py
│   │   ├── tag_services.pyc
│   │   ├── techSolution_services.py
│   │   ├── techSolution_services.pyc
│   │   ├── testmail_service.py
│   │   ├── testmail_service.pyc
│   │   ├── user_services.py
│   │   ├── user_services.pyc
│   │   ├── watch_services.py
│   │   └── watch_services.pyc
│   ├── static
│   │   └── media
│   │       ├── issue_images
│   │       │   └── logo_1_20231020172950.png
│   │       ├── media_images
│   │       │   └── image_None_20240723145341.png
│   │       └── project_images
│   │           ├── image3x1_37_20231019175716.jpg
│   │           └── image3x1_37_20231019180103.jpg
│   ├── templatetags
│   │   ├── extras.py
│   │   ├── extras.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── markdown.py
│   │   ├── markdown.pyc
│   │   ├── pagination.py
│   │   └── pagination.pyc
│   ├── tests
│   │   ├── helpers
│   │   │   ├── email_asserts.py
│   │   │   ├── email_asserts.pyc
│   │   │   ├── __init__.py
│   │   │   ├── __init__.pyc
│   │   │   ├── mockers.py
│   │   │   ├── mockers.pyc
│   │   │   ├── test_data.py
│   │   │   └── test_data.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── test_api_views.py
│   │   ├── test_api_views.pyc
│   │   ├── test_bitcoin.py
│   │   ├── test_bitcoin.pyc
│   │   ├── test_feedback_views.py
│   │   ├── test_feedback_views.pyc
│   │   ├── test_frespo_utils.py
│   │   ├── test_frespo_utils.pyc
│   │   ├── test_issue_views.py
│   │   ├── test_issue_views.pyc
│   │   ├── test_mail_notifications.py
│   │   ├── test_mail_notifications.pyc
│   │   ├── test_model_entities.py
│   │   ├── test_model_entities.pyc
│   │   ├── test_pagination.py
│   │   ├── test_pagination.pyc
│   │   ├── test_paypal_services.py
│   │   ├── test_paypal_services.pyc
│   │   ├── test_project_views.py
│   │   ├── test_project_views.pyc
│   │   ├── test_registration.py
│   │   ├── test_registration.pyc
│   │   ├── test_stats_views.py
│   │   ├── test_stats_views.pyc
│   │   ├── tests_trackerintegration.py
│   │   ├── tests_trackerintegration.pyc
│   │   ├── test_user_services.py
│   │   ├── test_user_services.pyc
│   │   ├── test_user_views.py
│   │   ├── test_user_views.pyc
│   │   ├── test_watch_services.py
│   │   ├── test_watch_services.pyc
│   │   ├── test_watch_views.py
│   │   └── test_watch_views.pyc
│   ├── urls
│   │   ├── api_urls.py
│   │   ├── api_urls.pyc
│   │   ├── feedback_urls.py
│   │   ├── feedback_urls.pyc
│   │   ├── github_hook_urls.py
│   │   ├── github_hook_urls.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── issue_urls.py
│   │   ├── issue_urls.pyc
│   │   ├── offer_urls.py
│   │   ├── offer_urls.pyc
│   │   ├── payment_urls.py
│   │   ├── payment_urls.pyc
│   │   ├── project_urls.py
│   │   ├── project_urls.pyc
│   │   ├── solution_urls.py
│   │   ├── solution_urls.pyc
│   │   ├── user_urls.py
│   │   └── user_urls.pyc
│   ├── utils
│   │   ├── bitcoin_validation.py
│   │   ├── bitcoin_validation.pyc
│   │   ├── djangology_utils.py
│   │   ├── djangology_utils.pyc
│   │   ├── frespo_utils.py
│   │   ├── frespo_utils.pyc
│   │   ├── __init__.py
│   │   ├── __init__.pyc
│   │   ├── paypal_adapter.py
│   │   ├── paypal_adapter.pyc
│   │   ├── trackers_adapter.py
│   │   └── trackers_adapter.pyc
│   └── views
│       ├── api_views.py
│       ├── api_views.pyc
│       ├── bitcoin_views.py
│       ├── bitcoin_views.pyc
│       ├── comment_views.py
│       ├── comment_views.pyc
│       ├── feedback_views.py
│       ├── feedback_views.pyc
│       ├── github_hook_views.py
│       ├── github_hook_views.pyc
│       ├── __init__.py
│       ├── __init__.pyc
│       ├── issue_views.py
│       ├── issue_views.pyc
│       ├── json_views.py
│       ├── json_views.pyc
│       ├── main_views.py
│       ├── main_views.pyc
│       ├── media_views.py
│       ├── media_views.pyc
│       ├── payment_views.py
│       ├── payment_views.pyc
│       ├── paypal_views.py
│       ├── paypal_views.pyc
│       ├── project_views.py
│       ├── project_views.pyc
│       ├── registration_views.py
│       ├── revision_views.py
│       ├── revision_views.pyc
│       ├── techSolution_views.py
│       ├── techSolution_views.pyc
│       ├── user_views.py
│       ├── user_views.pyc
│       └── watch_views.py
├── coverage.sh
├── devenv_settings.sh
├── frespo
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── settings.py
│   ├── settings.pyc
│   ├── socialauth_pipeline.py
│   ├── urls.py
│   ├── urls.pyc
│   ├── wsgi.py
│   └── wsgi.pyc
├── frespo_currencies
│   ├── currency_service.py
│   ├── currency_service.pyc
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0001_initial.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── models.py
│   ├── models.pyc
│   └── tests.py
├── gen_datamigration.sh
├── gen_migrations.sh
├── gh_frespo_integration
│   ├── __init__.py
│   ├── __init__.pyc
│   ├── management
│   │   ├── commands
│   │   │   ├── github_sponsorthis.py
│   │   │   └── __init__.py
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── migrations
│   │   ├── 0001_initial.py
│   │   ├── 0001_initial.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── models.py
│   ├── models.pyc
│   ├── services
│   │   ├── github_services.py
│   │   ├── github_services.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   ├── tests
│   │   ├── __init__.py
│   │   └── test_github_adapter.py
│   ├── urls.py
│   ├── urls.pyc
│   ├── utils
│   │   ├── github_adapter.py
│   │   ├── github_adapter.pyc
│   │   ├── __init__.py
│   │   └── __init__.pyc
│   └── views
│       ├── __init__.py
│       ├── __init__.pyc
│       ├── main_views.py
│       └── main_views.pyc
├── github_sponsorthis.sh
├── grunt
│   ├── aliases.yaml
│   ├── clean.js
│   ├── concat.js
│   ├── ngtemplates.js
│   └── notused
│       ├── copy.js
│       ├── karma.js
│       ├── ngmin.js
│       ├── uglify.js
│       └── watch.js
├── Gruntfile.js
├── kabum.sh
├── key.pem
├── locale
│   ├── de
│   │   └── LC_MESSAGES
│   │       └── django.po
│   ├── es
│   │   └── LC_MESSAGES
│   │       ├── django.mo
│   │       ├── django.po
│   │       ├── es.mo
│   │       └── es.po
│   └── pt_BR
│       └── LC_MESSAGES
│           ├── django.mo
│           └── django.po
├── logs
│   ├── empty.txt
│   └── frespo.log
├── makemessages.sh
├── manage.py
├── migrate.sh
├── package.json
├── populaTestes.sh
├── refillReceiveAddressPool.sh
├── sobe.sh
├── start_sandbox.sh
├── statfiles
│   ├── __init__.py
│   ├── __init__.pyc
│   └── static
│       ├── admin
│       │   ├── css
│       │   │   ├── base.css
│       │   │   ├── changelists.css
│       │   │   ├── dashboard.css
│       │   │   ├── forms.css
│       │   │   ├── ie.css
│       │   │   ├── login.css
│       │   │   ├── rtl.css
│       │   │   └── widgets.css
│       │   ├── img
│       │   │   ├── changelist-bg.gif
│       │   │   ├── changelist-bg_rtl.gif
│       │   │   ├── chooser-bg.gif
│       │   │   ├── chooser_stacked-bg.gif
│       │   │   ├── default-bg.gif
│       │   │   ├── default-bg-reverse.gif
│       │   │   ├── deleted-overlay.gif
│       │   │   ├── gis
│       │   │   │   ├── move_vertex_off.png
│       │   │   │   └── move_vertex_on.png
│       │   │   ├── icon_addlink.gif
│       │   │   ├── icon_alert.gif
│       │   │   ├── icon_calendar.gif
│       │   │   ├── icon_changelink.gif
│       │   │   ├── icon_clock.gif
│       │   │   ├── icon_deletelink.gif
│       │   │   ├── icon_error.gif
│       │   │   ├── icon-no.gif
│       │   │   ├── icon_searchbox.png
│       │   │   ├── icon_success.gif
│       │   │   ├── icon-unknown.gif
│       │   │   ├── icon-yes.gif
│       │   │   ├── inline-delete-8bit.png
│       │   │   ├── inline-delete.png
│       │   │   ├── inline-restore-8bit.png
│       │   │   ├── inline-restore.png
│       │   │   ├── inline-splitter-bg.gif
│       │   │   ├── nav-bg.gif
│       │   │   ├── nav-bg-grabber.gif
│       │   │   ├── nav-bg-reverse.gif
│       │   │   ├── nav-bg-selected.gif
│       │   │   ├── selector-icons.gif
│       │   │   ├── selector-search.gif
│       │   │   ├── sorting-icons.gif
│       │   │   ├── tool-left.gif
│       │   │   ├── tool-left_over.gif
│       │   │   ├── tool-right.gif
│       │   │   ├── tool-right_over.gif
│       │   │   ├── tooltag-add.gif
│       │   │   ├── tooltag-add_over.gif
│       │   │   ├── tooltag-arrowright.gif
│       │   │   └── tooltag-arrowright_over.gif
│       │   └── js
│       │       ├── actions.js
│       │       ├── actions.min.js
│       │       ├── admin
│       │       │   ├── DateTimeShortcuts.js
│       │       │   ├── ordering.js
│       │       │   └── RelatedObjectLookups.js
│       │       ├── calendar.js
│       │       ├── collapse.js
│       │       ├── collapse.min.js
│       │       ├── compress.py
│       │       ├── core.js
│       │       ├── getElementsBySelector.js
│       │       ├── inlines.js
│       │       ├── inlines.min.js
│       │       ├── jquery.init.js
│       │       ├── jquery.js
│       │       ├── jquery.min.js
│       │       ├── LICENSE-JQUERY.txt
│       │       ├── prepopulate.js
│       │       ├── prepopulate.min.js
│       │       ├── SelectBox.js
│       │       ├── SelectFilter2.js
│       │       ├── timeparse.js
│       │       └── urlify.js
│       ├── bootstrap
│       │   ├── css
│       │   │   ├── bootstrap.css
│       │   │   ├── bootstrap.min.css
│       │   │   └── diff2html.min.css
│       │   ├── img
│       │   │   ├── glyphicons-halflings.png
│       │   │   └── glyphicons-halflings-white.png
│       │   └── js
│       │       ├── bootstrap.js
│       │       ├── bootstrap.min.js
│       │       ├── bootstrap-typeahead.js
│       │       ├── diff2html.min.js
│       │       ├── diff2html-ui.min.js
│       │       ├── difflib-browser.js
│       │       ├── jquery.js
│       │       ├── jquery.maskedinput-1.3.min.js
│       │       ├── jquery.min.js
│       │       ├── jquery.numeric.js
│       │       ├── markdown-it.min.js
│       │       ├── markdown-it-task-lists.min.js
│       │       └── showdown.js
│       ├── bootstrap3
│       │   ├── css
│       │   │   ├── bootstrap.css
│       │   │   ├── bootstrap.min.css
│       │   │   ├── bootstrap-theme.css
│       │   │   └── bootstrap-theme.min.css
│       │   ├── fonts
│       │   │   ├── glyphicons-halflings-regular.eot
│       │   │   ├── glyphicons-halflings-regular.svg
│       │   │   ├── glyphicons-halflings-regular.ttf
│       │   │   └── glyphicons-halflings-regular.woff
│       │   └── js
│       │       ├── bootstrap.js
│       │       └── bootstrap.min.js
│       ├── css
│       │   ├── frespo.css
│       │   ├── highlighting.css
│       │   └── projectfavs.css
│       ├── css2
│       │   ├── bootstrap_ish.css
│       │   ├── djangology_style.css
│       │   ├── freedom_bootstrap.css
│       │   ├── freedom_bootstrap_tony.css
│       │   ├── fs_responsive.css
│       │   ├── fs_structure.css
│       │   ├── fs_style.css
│       │   ├── img
│       │   │   ├── checkbox_checked.png
│       │   │   ├── checkbox_unchecked.png
│       │   │   ├── glyphicons-halflings.png
│       │   │   ├── glyphicons-halflings-white.png
│       │   │   ├── radio_checked.png
│       │   │   └── radio_unchecked.png
│       │   ├── markdown.css
│       │   └── reset.css
│       ├── favicon.ico
│       ├── favicon_old.ico
│       ├── font-awesome
│       │   ├── css
│       │   │   ├── font-awesome.css
│       │   │   └── font-awesome.min.css
│       │   ├── fonts
│       │   │   ├── FontAwesome.otf
│       │   │   ├── fontawesome-webfont.eot
│       │   │   ├── fontawesome-webfont.svg
│       │   │   ├── fontawesome-webfont.ttf
│       │   │   ├── fontawesome-webfont.woff
│       │   │   └── fontawesome-webfont.woff2
│       │   ├── HELP-US-OUT.txt
│       │   ├── less
│       │   │   ├── animated.less
│       │   │   ├── bordered-pulled.less
│       │   │   ├── core.less
│       │   │   ├── fixed-width.less
│       │   │   ├── font-awesome.less
│       │   │   ├── icons.less
│       │   │   ├── larger.less
│       │   │   ├── list.less
│       │   │   ├── mixins.less
│       │   │   ├── path.less
│       │   │   ├── rotated-flipped.less
│       │   │   ├── screen-reader.less
│       │   │   ├── stacked.less
│       │   │   └── variables.less
│       │   └── scss
│       │       ├── _animated.scss
│       │       ├── _bordered-pulled.scss
│       │       ├── _core.scss
│       │       ├── _fixed-width.scss
│       │       ├── font-awesome.scss
│       │       ├── _icons.scss
│       │       ├── _larger.scss
│       │       ├── _list.scss
│       │       ├── _mixins.scss
│       │       ├── _path.scss
│       │       ├── _rotated-flipped.scss
│       │       ├── _screen-reader.scss
│       │       ├── _stacked.scss
│       │       └── _variables.scss
│       ├── fonts
│       │   ├── glyphicons-halflings-regular.eot
│       │   ├── glyphicons-halflings-regular.svg
│       │   ├── glyphicons-halflings-regular.ttf
│       │   ├── glyphicons-halflings-regular.woff
│       │   ├── ubahn-light-webfont.eot
│       │   ├── ubahn-light-webfont.svg
│       │   ├── ubahn-light-webfont.ttf
│       │   ├── ubahn-light-webfont.woff
│       │   ├── ubahn-webfont.eot
│       │   ├── ubahn-webfont.svg
│       │   ├── ubahn-webfont.ttf
│       │   └── ubahn-webfont.woff
│       ├── FS_name.png
│       ├── google4e43bddf47aa0cf3.html
│       ├── img
│       │   ├── apres
│       │   │   ├── 01.png
│       │   │   ├── 02.png
│       │   │   ├── 03.png
│       │   │   ├── 04.png
│       │   │   ├── 05.png
│       │   │   ├── 06.png
│       │   │   ├── 07.png
│       │   │   ├── 08.png
│       │   │   ├── 09.png
│       │   │   ├── 10.png
│       │   │   ├── 11.png
│       │   │   ├── 12.png
│       │   │   ├── 13.png
│       │   │   ├── 14.png
│       │   │   ├── 15.png
│       │   │   ├── tony
│       │   │   │   ├── 01.png
│       │   │   │   └── 02.png
│       │   │   └── we_believe.png
│       │   ├── bitbucket.jpg
│       │   ├── bitbucket_small.png
│       │   ├── bitcoin_icon_32.png
│       │   ├── brazilianFlag.gif
│       │   ├── bug_wanted.jpg
│       │   ├── facebook-42.png
│       │   ├── facebook.gif
│       │   ├── facebook_small.png
│       │   ├── forkinho.png
│       │   ├── fs2.ico
│       │   ├── FS_horizontal_logo_colored_482.png
│       │   ├── fs.ico
│       │   ├── FS_logo_32x32.png
│       │   ├── FS_logo_512x512.png
│       │   ├── FS_logo_home.png
│       │   ├── FS_name.png
│       │   ├── github.png
│       │   ├── github_small.gif
│       │   ├── globe.png
│       │   ├── glyphicons_003_user.png
│       │   ├── Google+-42.jpg
│       │   ├── google_button.png
│       │   ├── google.gif
│       │   ├── google_small.png
│       │   ├── home
│       │   │   ├── imgFBProvisorio.png
│       │   │   └── logo.png
│       │   ├── icon-facebook.png
│       │   ├── imgFBProvisorio.png
│       │   ├── jsonarray.html
│       │   ├── loading.gif
│       │   ├── myopenid.png
│       │   ├── nopayment.png
│       │   ├── paypal_icon_32.png
│       │   ├── rss_icon.gif
│       │   ├── twitter-42.png
│       │   ├── twitter.png
│       │   ├── twitter_small.png
│       │   ├── user_128.png
│       │   ├── user_23.png
│       │   ├── user_48.png
│       │   ├── user.png
│       │   ├── yahoo.gif
│       │   └── yahoo_small.png
│       ├── img2
│       │   ├── ajax-loader.gif
│       │   ├── card_status_done.png
│       │   ├── card_status_open.png
│       │   ├── card_status_working.png
│       │   ├── default_project_logo.png
│       │   ├── fork_on_github.png
│       │   ├── fs_background.jpg
│       │   ├── fs_brand.png
│       │   ├── fs_delete_icon.png
│       │   ├── fs_done_icon.png
│       │   ├── fs_edit_icon.png
│       │   ├── fs_external_link_icon.png
│       │   ├── fs_link_icon.png
│       │   ├── fs_logo.png
│       │   ├── fs_tag_icon.png
│       │   ├── fs_unwatch_icon.png
│       │   ├── fs_watch_icon.png
│       │   ├── fw_logo.png
│       │   ├── github_logo.jpg
│       │   ├── glyphicons-halflings.png
│       │   ├── project_logos
│       │   │   ├── blender_logo.png
│       │   │   ├── debian_logo.png
│       │   │   ├── freedomsponsors_logo.png
│       │   │   └── jenkins_logo.png
│       │   ├── search_icon_menu.png
│       │   ├── search_icon.png
│       │   ├── social_login_facebook.png
│       │   ├── social_login_github.png
│       │   ├── social_login_google.png
│       │   ├── social_login_twitter.png
│       │   ├── social_logos
│       │   │   ├── facebook.png
│       │   │   ├── google.png
│       │   │   ├── rss.png
│       │   │   └── twitter.png
│       │   └── top_list_img.png
│       ├── js
│       │   ├── 2.5.3-crypto-sha256.js
│       │   ├── activitylist
│       │   │   ├── activitylist.html
│       │   │   └── activitylist.js
│       │   ├── angular.min.js
│       │   ├── angular-sanitize.min.js
│       │   ├── angularutils
│       │   │   ├── angularutils.js
│       │   │   ├── media-form-template.html
│       │   │   ├── sortHeader.html
│       │   │   ├── textarea-and-markdownpreview.html
│       │   │   ├── watch-issue.html
│       │   │   └── watch.js
│       │   ├── contenteditable
│       │   │   └── contenteditable.js
│       │   ├── fsapi.js
│       │   ├── fsapi_mock.js
│       │   ├── fsbase.js
│       │   ├── fslinks.js
│       │   ├── fslinks_sandbox.js
│       │   ├── fsutil.js
│       │   ├── issuecards
│       │   │   ├── issuecard_proposed.html
│       │   │   ├── issuecards.html
│       │   │   ├── issuecards.js
│       │   │   └── issuecard_sponsored.html
│       │   ├── jsbn2.js
│       │   ├── jsbn.js
│       │   ├── markdown-it-djangology-flavoured.js
│       │   ├── so
│       │   │   └── soapi.js
│       │   ├── tags
│       │   │   ├── tag_api.js
│       │   │   ├── tag_api_mock.js
│       │   │   ├── taglist.html
│       │   │   └── taglist.js
│       │   └── validate_bitcoin.js
│       ├── js-generated
│       │   ├── fs.js
│       │   └── fs-templates.js
│       ├── media
│       │   ├── empty
│       │   └── project_images
│       │       ├── image3x1_1_20130711020450.png
│       │       └── image3x1_1_20130711021828.png
│       ├── spa
│       │   ├── css
│       │   │   ├── fs.css
│       │   │   ├── lib.css
│       │   │   └── lib.min.css
│       │   └── js
│       │       ├── docs.js
│       │       ├── fsdocs.js
│       │       ├── fs.js
│       │       ├── lib.js
│       │       └── lib.min.js
│       └── speech2.html
├── templates
│   ├── 404.html
│   ├── 500.html
│   ├── core
│   │   ├── base.html
│   │   ├── empty_base.html
│   │   ├── navbar.html
│   │   └── popup
│   │       ├── popup_just_kickstarted.html
│   │       ├── popup_just_sponsored.html
│   │       └── popup_messages.html
│   ├── core2
│   │   ├── account_disabled.html
│   │   ├── add_issue.html
│   │   ├── add_issue_old.html
│   │   ├── admail.html
│   │   ├── base.html
│   │   ├── base_old.html
│   │   ├── bitcoin_disabled.html
│   │   ├── change_username.html
│   │   ├── comment_history.html
│   │   ├── developers.html
│   │   ├── empty_base.html
│   │   ├── faq.html
│   │   ├── feedback_form.html
│   │   ├── feedback.html
│   │   ├── history.html
│   │   ├── home.html
│   │   ├── include
│   │   │   ├── comment_add.html
│   │   │   ├── comment.html
│   │   │   ├── edit_comment_form.html
│   │   │   ├── footer.html
│   │   │   ├── generic_typeahead.html
│   │   │   ├── green_and_orange.html
│   │   │   ├── history_list.html
│   │   │   ├── issue_grid.html
│   │   │   ├── issue_offer_action_bar.html
│   │   │   ├── media_carrousel.html
│   │   │   ├── media_form.html
│   │   │   ├── media.html
│   │   │   ├── navbar.html
│   │   │   ├── offer_form_fields.html
│   │   │   ├── offer_grid.html
│   │   │   ├── openid_login.html
│   │   │   ├── solution_grid.html
│   │   │   ├── techSolution_form.html
│   │   │   ├── techSolution.html
│   │   │   └── watch_toggle_button.html
│   │   ├── issue.html
│   │   ├── issue_list.html
│   │   ├── jslic.html
│   │   ├── mailtest.html
│   │   ├── maintainance.html
│   │   ├── myissues.html
│   │   ├── newbase.html
│   │   ├── newhome.html
│   │   ├── payment.html
│   │   ├── payment_list.html
│   │   ├── pay_offer_angular.html
│   │   ├── paypal_canceled.html
│   │   ├── paypal_confirmed.html
│   │   ├── popup
│   │   │   ├── popup_just_kickstarted.html
│   │   │   ├── popup_just_sponsored.html
│   │   │   └── popup_messages.html
│   │   ├── project_edit.html
│   │   ├── project.html
│   │   ├── project_list.html
│   │   ├── robots.txt
│   │   ├── sitemap.xml
│   │   ├── solution.html
│   │   ├── stats.html
│   │   ├── useredit.html
│   │   ├── user.html
│   │   ├── user_inactive.html
│   │   ├── userlist.html
│   │   └── waitPaymentBitcoin.html
│   ├── email
│   │   ├── acceptingpayments.html
│   │   ├── base.html
│   │   ├── bitcoin_payment_was_sent_to_programmers_and_is_waiting_confirmation.html
│   │   ├── comment_added.html
│   │   ├── farewell.html
│   │   ├── include
│   │   │   └── email_settings_notice.html
│   │   ├── offeradded.html
│   │   ├── offerchanged.html
│   │   ├── offerrevoked.html
│   │   ├── payment_made.html
│   │   ├── payment_received.html
│   │   ├── payment_sent.html
│   │   ├── project_edited.html
│   │   ├── project_tag_added.html
│   │   ├── project_tag_removed.html
│   │   ├── welcome.html
│   │   ├── workbegun.html
│   │   ├── workdone.html
│   │   ├── workdone_sponsor.html_old
│   │   └── workstopped.html
│   ├── emailmgr
│   │   ├── emailmgr_activation_message.txt
│   │   ├── emailmgr_activation_subject.txt
│   │   └── emailmgr_email_list.html
│   ├── github
│   │   └── configure.html
│   ├── pagination
│   │   └── pagination.html
│   ├── registration
│   │   ├── activate.html
│   │   ├── activation_complete.html
│   │   ├── activation_email_subject.txt
│   │   ├── activation_email.txt
│   │   ├── base.html
│   │   ├── login_error.html
│   │   ├── login.html
│   │   ├── login_old.html
│   │   ├── logout.html
│   │   ├── password_change_done.html
│   │   ├── password_change_form.html
│   │   ├── password_reset_complete.html
│   │   ├── password_reset_confirm.html
│   │   ├── password_reset_done.html
│   │   ├── password_reset_email.html
│   │   ├── password_reset_form.html
│   │   ├── registration_complete.html
│   │   └── registration_form.html
│   ├── sandbox
│   │   ├── adropdown.html
│   │   ├── amodal.html
│   │   ├── apopover.html
│   │   ├── atabnav.html
│   │   ├── base.html
│   │   ├── empty_base.html
│   │   ├── include
│   │   │   ├── footer.html
│   │   │   ├── navbar.html
│   │   │   └── user_navbar.html
│   │   ├── project.html
│   │   ├── search.html
│   │   └── user.html
│   └── spa.html
├── test_all.sh
├── test_splinter.sh
└── upload_travis_reports.sh

103 directories, 739 files
