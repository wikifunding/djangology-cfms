# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2024-12-10 15:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_issuecomment_language'),
    ]

    operations = [
        migrations.AddField(
            model_name='techsolutioncomment',
            name='language',
            field=models.CharField(default=b'', max_length=5),
        ),
    ]
