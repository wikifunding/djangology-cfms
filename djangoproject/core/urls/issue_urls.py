from django.conf.urls import url
from core.views import issue_views
from core.views import comment_views
from core.views import media_views
from core.views import revision_views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^rss$', issue_views.listIssuesFeed),
    url(r'^sponsor/submit$', issue_views.sponsorIssue),
    url(r'^sponsor$', issue_views.addIssueForm),
    url(r'^add/submit$', issue_views.addIssue),
    url(r'^kickstart/submit$', issue_views.kickstartIssue),
    url(r'^add/$', issue_views.addIssueForm),
    url(r'^edit/submit$', issue_views.editIssue),
    url(r'^saveProjectName$', issue_views.saveProjectName),
    # Djangology urls
    url(r'^(?P<issue_id>\d+)/history', revision_views.IssueHistory.as_view()),
    url(r'^(?P<issue_id>\d+)/compare', revision_views.IssueCompare.as_view()),
    # End djangology urls
    url(r'^(?P<issue_id>\d+)/$', issue_views.viewIssue),
    url(r'^(?P<issue_id>\d+)/.*$', issue_views.viewIssue),
    url(r'^new/(?P<issue_id>\d+)/.*$', issue_views.viewIssueNew),
    url(r'^vote$', issue_views.vote),
    url(r'^vote_solution$', issue_views.voteSolution),
]

urlpatterns += [
    url(r'^comment/add/submit$', comment_views.addIssueComment),
    url(r'^comment/edit/submit$', comment_views.editIssueComment),
    url(r'^comment/(?P<comment_id>\d+)/history$', comment_views.viewIssueCommentHistory),
]

# Filter issues by tag
urlpatterns += [
    url(r'^tag/(?P<tag_slug>[-\w]+)/$', issue_views.listIssues, name='issues_by_tag'),
]

# Djangology urls
urlpatterns += [
    url(r'^media/([0-9]+)', media_views.MediaDetail.as_view()),
    url(r'^media/', media_views.MediaList.as_view()),
]
#